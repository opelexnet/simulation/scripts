#!/usr/bin/env python3
# file: 3ph_PWMpulses.py
# Author    : Najath Abdul Azeez
# Copyright : http://opelex.net
# License   : See LICENSE file
"""
Illustration of gate pulses in 3-phase PWM schemes
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import RegularPolygon
from matplotlib.widgets import Slider, RadioButtons


##############################################################################
# Functions for initializing and updating global variables                   #
##############################################################################
# Function for initializing global variables
def initGlobalVars():
    global xd, x, m, theta, thetaR, zeros
    global PWM, c_t, t, ones
    # Angle in degrees and radians, with step size of 0.1
    xd = np.array(np.arange(0, 361, 0.1))
    x  = xd * np.pi / 180
    zeros = np.zeros_like(xd)
    # Define PWM technique
    PWM = 'Sine'
    # Initial modulation index and angle
    m = 0.80
    theta = 30
    # Triangular carrier and it's time base
    c_t  = np.concatenate([(np.arange(-1, 1, 0.001)), np.arange(1, -1, -0.001)])
    t    = np.array(np.arange(0, 1, 0.00025))
    ones = np.ones_like(t)


# Function for calculating 3-phase reference sine waves
def calcRefSines():
    global a, b, c
    # Create 3-phase sine waves
    a = m * np.cos(x)
    b = m * np.cos(x - 2 * (np.pi / 3))
    c = m * np.cos(x - 4 * (np.pi / 3))


# Function for calculating Vm
def calcVm():
    global thetaR, Vm
    # Calculate theta in radians
    thetaR = theta * (np.pi / 180)
    # Vector Vm
    Vm = 0.75 * m * np.exp(thetaR * 1j)


# Function for calculating offset corresponding to the selected PWM technique
def calcOffset():
    global offset
    # Find max, min
    mx = (np.array([a, b, c])).max(axis=0)
    mn = (np.array([a, b, c])).min(axis=0)
    # Calculate offset for SVPWM
    offsetSV = (0 - (mx + mn)) / 2
    # Calculate offset for high clamp and low clamp
    offsetH =  1 - mx
    offsetL = -1 - mn
    # Calculate offset for DPWM
    comp = mx > -mn
    offsetD = ((offsetH) * comp) + ((offsetL) * (~comp))
    # Select offset based on PWM technique
    offset = {
        'Sine'    : zeros,
        'SVPWM'   : offsetSV,
        'DPWM'    : offsetD,
        'HiClamp' : offsetH,
        'LowClamp': offsetL,
    }[PWM]


# Sample modulating wave and generate the gate pulses
def calcGatePulses():
    global sA, sB, sC
    global gA, gB, gC
    thetaIndex = int(theta * 10)
    # Sample the three modulating waves
    sA = (a[thetaIndex] + offset[thetaIndex]) * ones
    sB = (b[thetaIndex] + offset[thetaIndex]) * ones
    sC = (c[thetaIndex] + offset[thetaIndex]) * ones
    # Generate the gate pulses. Scale and offset for plotting.
    gA = (sA >= c_t) * 0.2 + 0.4
    gB = (sB >= c_t) * 0.2 + 0.0
    gC = (sC >= c_t) * 0.2 - 0.4


# Function for updating reference sine waves
def updateRwaves():
    calcRefSines()
    updateRefPlots()


# Function for updating modulating waves
def updateMwaves():
    calcOffset()
    updateMwavePlots()


# Function for updating Vm
def updateVm():
    calcVm()
    updateVmPlots()
    updateTimings()


# Function for updating gate pulses and timing plots
def updateTimings():
    calcGatePulses()
    updateTimingPlots()


##############################################################################
# Functions for creating and updating plots                                  #
##############################################################################
# Function for creating figures and required subplots
def createFig():
    # Create a 20" by 8" figure
    global fig
    fig = plt.figure()
    fig.set_size_inches(20, 8, forward=True)
    # Create 5 subplots
    global ax1, ax2, ax3, ax4, ax5
    ax1 = plt.subplot2grid((2, 5), (0, 0), colspan=2, rowspan=2)
    ax2 = plt.subplot2grid((2, 5), (0, 2))
    ax3 = plt.subplot2grid((2, 5), (1, 2))
    ax4 = plt.subplot2grid((2, 5), (0, 3), colspan=2)
    ax5 = plt.subplot2grid((2, 5), (1, 3), colspan=2)


# Plot all waveforms and get the handles
def initPlots():
    # Hexagon in SV diagram
    Hex = RegularPolygon((0.0, 0.0), 6, 1.0, np.pi / 6, alpha=0.8, fill=False)
    ax1.add_patch(Hex)
    # Vm arrow in SV diagram
    global Vm_arw
    Vm_arw  = ax1.arrow(0, 0, Vm.real, Vm.imag, width=0.025, head_width=0.05, color='.35', length_includes_head=True)
    # Sine waves with offset
    global sineAO, sineBO, sineCO
    sineAO, = ax2.plot(xd, a + offset, 'r')
    sineBO, = ax2.plot(xd, b + offset, 'y')
    sineCO, = ax2.plot(xd, c + offset, 'b')
    # Reference sine waves
    global sineA, sineB, sineC
    sineA, = ax3.plot(xd, a, 'r')
    sineB, = ax3.plot(xd, b, 'y')
    sineC, = ax3.plot(xd, c, 'b')
    # vLine for selected sample
    global sLine, sLineO
    sLineO, = ax2.plot([theta, theta], [-1.3, 1.3], 'k')
    sLine, = ax3.plot([theta, theta], [-1.3, 1.3], 'k')
    # Carrier, sample and gate pulses
    global carrier, smplA, smplB, smplC, pulsA, pulsB, pulsC
    carrier, = ax4.plot(t, c_t, 'k')
    smplA, = ax4.plot(t, sA, 'r')
    smplB, = ax4.plot(t, sB, 'y')
    smplC, = ax4.plot(t, sC, 'b')
    pulsA, = ax5.plot(t, gA, 'r')
    pulsB, = ax5.plot(t, gB, 'y')
    pulsC, = ax5.plot(t, gC, 'b')

# Function for plot beautification
def plotChromes():
    xticks = range(60, 360, 60)
    # ax1 - SV diagram
    ax1.set_ylim(-1.3, 1.3)
    ax1.set_xlim(-1.3, 1.3)
    # ax2 - modulating waves
    ax2.set_xticks(xticks)
    ax2.grid(True, 'both')
    ax2.set_ylim(-1.3, 1.3)
    ax2.set_xlim(0, 360)
    # ax3 - reference sine waves
    ax3.set_xticks(xticks)
    ax3.grid(True, 'both')
    ax3.set_ylim(-1.3, 1.3)
    ax3.set_xlim(0, 360)
    # ax4 - triangular carrier and 3-ph samples
    ax4.set_xticks([0.5, 1])
    ax4.grid(True, axis='x')
    ax4.set_ylim(-1.3, 1.3)
    ax4.set_xlim(0, 1)
    # ax5 - 3-ph gate pulses
    ax5.set_xticks([0.5, 1])
    ax5.set_yticks([])
    ax5.grid(True, axis='x')
    ax5.set_ylim(-0.5, 0.7)
    ax5.set_xlim(0, 1)
    # Set plot titles
    ax1.set_title("SV diagram")
    ax2.set_title("Modulating waves")
    ax3.set_title("Reference waves")
    # Adjust plot sizes to avoid overlaps
    plt.tight_layout()


# Function for updating reference sine waves plots
def updateRefPlots():
    sineA.set_ydata(a)
    sineB.set_ydata(b)
    sineC.set_ydata(c)


# Function for updating modulating waves plots
def updateMwavePlots():
    sineAO.set_ydata(a + offset)
    sineBO.set_ydata(b + offset)
    sineCO.set_ydata(c + offset)


# Function for updating Vm plots
def updateVmPlots():
    global Vm_arw
    Vm_arw.remove()
    Vm_arw = ax1.arrow(0, 0, Vm.real, Vm.imag, width=0.025, head_width=0.05, color='.35', length_includes_head=True)
    sLine.set_xdata([theta, theta])
    sLineO.set_xdata([theta, theta])


# Function for updating timing plots
def updateTimingPlots():
    smplA.set_ydata(sA)
    smplB.set_ydata(sB)
    smplC.set_ydata(sC)
    pulsA.set_ydata(gA)
    pulsB.set_ydata(gB)
    pulsC.set_ydata(gC)


##############################################################################
# HMI button initialization and call back functions                          #
##############################################################################
def initHMI():
    global sMindex, sTheta, rPWM
    # Make space in fig bottom, for placing the sliders and radio button
    fig.subplots_adjust(bottom=0.15)
    # Define colors to be used
    axbgcolor = 'lightgoldenrodyellow'
    # Create slider for modulation index (m)
    axMindex = plt.axes([0.05, 0.025, 0.125, 0.02], facecolor=axbgcolor)
    sMindex  = Slider(axMindex, 'm-index', 0.1, 1.15, valinit=m)
    # Connect function for updating change in "Modulation index"
    sMindex.on_changed(updateMagnitude)
    # Create slider for angle (theta)
    axTheta = plt.axes([0.05, 0.070, 0.125, 0.02], facecolor=axbgcolor)
    sTheta  = Slider(axTheta, 'Theta', 0, 360, valinit=theta)
    # Connect function for updating change in Vm
    sTheta.on_changed(updateTheta)
    # Create radio button for selecting PWM technique
    axPWM = plt.axes([0.25, 0.000, 0.125, 0.15], facecolor=axbgcolor)
    rPWM  = RadioButtons(axPWM, ('Sine', 'SVPWM', 'DPWM', 'HiClamp', 'LowClamp'), active=0)
    # Connect function for updating change in PWM
    rPWM.on_clicked(updatePWM)
# capture mouse events
    global cid
    cid = fig.canvas.mpl_connect('button_press_event', on_press)


# Function for updating magnitude
def updateMagnitude(mNew):
    global m
    m = mNew
    updateRwaves()
    updateMwaves()
    updateVm()


# Function for updating theta
def updateTheta(thetaNew):
    global theta, thetaR, Vm
    theta = thetaNew
    updateVm()


# Function for updating PWM
def updatePWM(PWMnew):
    global PWM
    PWM = PWMnew
    updateMwaves()
    updateTimings()


# Update plots on mouse press in SV subplot
def on_press(event):
    if (event.inaxes is ax1):
        VmNew = event.xdata + 1j*event.ydata
        global m, theta
        m = np.absolute(VmNew)/(0.75)
        theta = np.angle(VmNew,deg=1)%360
        sMindex.val = m
        sTheta.val = theta
        updateRwaves()
        updateMwaves()
        updateVm()
        fig.canvas.draw_idle()


##############################################################################
# Master initialization function                                             #
##############################################################################
def init():
    initGlobalVars()
    calcRefSines()
    calcOffset()
    calcVm()
    calcGatePulses()
    createFig()
    initPlots()
    plotChromes()
    initHMI()


##############################################################################
# Call initialization function and show the plot                             #
##############################################################################
init()
plt.show()
