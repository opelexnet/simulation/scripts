#!/usr/bin/env python3
# file: 3ph_PWMcarriers.py
# Author    : Najath Abdul Azeez
# Copyright : http://opelex.net
# License   : See LICENSE file
"""
Side-by-side illustration of popular carrier waves in 3-phase PWM schemes
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

# Create figure and three subplots stacked vertically
fig = plt.figure()
ax1 = fig.add_subplot(311)
ax2 = fig.add_subplot(312)
ax3 = fig.add_subplot(313)

# Angle in degrees and radians, with step size of 0.1
xd = np.array(np.arange(0, 361, 0.1))
x  = xd * np.pi / 180

# Initial modulation index
m = 0.8

# Create 3-phase sine waves
a = m * np.sin(x)
b = m * np.sin(x - 2 * np.pi / 3)
c = m * np.sin(x - 4 * np.pi / 3)

# Find max, min for offset computation
mx = (np.array([a, b, c])).max(axis=0)
mn = (np.array([a, b, c])).min(axis=0)

# Calculate offset for SVPWM
offset = (0 - (mx + mn)) / 2

# Calculate offset for DPWM
comp = mx > -mn
offsetD = ((1 - mx) * comp) + ((-1 - mn) * (~comp))

# Plot all waveforms and get the handles
sineA,  = ax1.plot(xd, a, 'r')
sineB,  = ax1.plot(xd, b, 'y')
sineC,  = ax1.plot(xd, c, 'b')
svpwmA, = ax2.plot(xd, a + offset, 'r')
svpwmB, = ax2.plot(xd, b + offset, 'y')
svpwmC, = ax2.plot(xd, c + offset, 'b')
svpwmO, = ax2.plot(xd, offset, 'k')
dpwmA,  = ax3.plot(xd, a + offsetD, 'r')
dpwmB,  = ax3.plot(xd, b + offsetD, 'y')
dpwmC,  = ax3.plot(xd, c + offsetD, 'b')
dpwmO,  = ax3.plot(xd, offsetD, 'k')

# Plot beautification
xticks = range(0, 360, 30)
ax1.set_xticks(xticks)
ax1.grid(True, 'both')
ax1.set_ylim(-1.3, 1.3)
ax1.set_xlim(0, 360)

ax2.set_xticks(xticks)
ax2.grid(True, 'both')
ax2.set_ylim(-1.3, 1.3)
ax2.set_xlim(0, 360)

ax3.set_xticks(xticks)
ax3.grid(True, 'both')
ax3.set_ylim(-1.3, 1.3)
ax3.set_xlim(0, 360)

# Set plot titles
ax1.set_title("Sine")
ax2.set_title("SVPWM")
ax3.set_title("DPWM")

# Adjust plot sizes to avoid overlaps
plt.tight_layout()

# Make space in the bottom, for placing the "Modulation index" Slider
fig.subplots_adjust(bottom=0.15)


# Function for updating the waveforms, using the new modulation index 'm'
def update(m):
    a  = m * np.sin(x)
    b  = m * np.sin(x - 2 * np.pi / 3)
    c  = m * np.sin(x - 4 * np.pi / 3)
    mx = (np.array([a, b, c])).max(axis=0)
    mn = (np.array([a, b, c])).min(axis=0)
    offset = (0 - (mx + mn)) / 2
    comp = mx > -mn
    offsetD = ((1 - mx) * comp) + ((-1 - mn) * (~comp))
    sineA.set_ydata(a)
    sineB.set_ydata(b)
    sineC.set_ydata(c)
    svpwmA.set_ydata(a + offset)
    svpwmB.set_ydata(b + offset)
    svpwmC.set_ydata(c + offset)
    svpwmO.set_ydata(offset)
    dpwmA.set_ydata(a + offsetD)
    dpwmB.set_ydata(b + offsetD)
    dpwmC.set_ydata(c + offsetD)
    dpwmO.set_ydata(offsetD)
    fig.canvas.draw_idle()


# Create a slider for "Modulation index"
axcolor = 'lightgoldenrodyellow'
axMindex = plt.axes([0.25, 0.05, 0.65, 0.03], facecolor=axcolor)
sMindex = Slider(axMindex, 'Modulation index', 0.1, 1.15, valinit=m)

# Update waveforms on change in "Modulation index"
sMindex.on_changed(update)

plt.show()
