#!/usr/bin/env python3
# Author    : Najath Abdul Azeez
# Copyright : http://opelex.net
# License   : See LICENSE file
"""
Animation of Current Error Space Vector (CESV) in a 3-phase inverter
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from itertools import accumulate

##############################################
# Configurations and Settings
##############################################
# Electrical
Vdc       = 1             # DC-link voltage. Normalized to 1.
f_rated   = 50            # Rated frequency
f         = 20            # Frequency of reference voltage vector
kf        = 0.987         # Flux (Vbyf) constant
# Inverter
kv        = 1             # Ratio of active vector to DC-link
N         = 6             # Number of sides in SV polygon
tilt      = 1             # SV polygon tilted or not
sct       = 0             # Sector for plotting CESV
# Carrier and sampling
Ts        = 100           # Number of samples in one half of carrier wave
thetaStep = 5             # Angle between two reference vector samples (degrees)

##############################################
# Initial calculations
##############################################
# Calculate angle between active vectors (delta)
delta = (2 * np.pi) / N
# Calculate angle of first vector of the current sector (alpha)
alpha = ((-delta / 2) * tilt) + (sct * delta)

# Calculate the vectors of the sector under consideration
V = [ 0 + 0j,
      kv * Vdc * np.exp(alpha * 1j),
      kv * Vdc * np.exp((alpha + delta) * 1j),
      0 + 0j
    ]

# Calculate modulation index (m) and magnitude of reference vector (Vm)
m  = kf * f / f_rated
Vm =  m * Vdc

# Initialize CESV array
cesv = np.array([], dtype=complex)

##############################################
# Define plot params
##############################################
# Carrier wave marker params
tm_params = [{'color' : 'm', 'markeredgecolor' : 'm', 'marker' : 'v'},
             {'color' : 'g', 'markeredgecolor' : 'g', 'marker' : 'v'},
             {'color' : 'b', 'markeredgecolor' : 'b', 'marker' : 'v'},
             {'color' : 'm', 'markeredgecolor' : 'm', 'marker' : 'v'}]
# Arrow params
arw_params  = {'width' : 0.025, 'head_width' : .05, 'length_includes_head' : True}
Vsa_params  = {'color' : 'k', **arw_params}
V1a_params  = {'color' : '0.35', **arw_params}
eVa_params = [{'color' : 'm', 'alpha' : 0.7, **arw_params},
              {'color' : 'g', 'alpha' : 0.7, **arw_params},
              {'color' : 'b', 'alpha' : 0.7, **arw_params},
              {'color' : 'm', 'alpha' : 0.7, **arw_params}]

##############################################
# Create & initialize figure and subplots
##############################################
fig = plt.figure()
fig.set_size_inches(16, 9, forward=True)
fig.subplots_adjust(top=0.95, bottom=0.03, left=0.03, right=0.97)

# Create subplots
axSV = fig.add_subplot(224) # Voltage SV
axCW = fig.add_subplot(223) # Carrier wave
axCE = fig.add_subplot(211) # Current error SV

# Set plot titles
axSV.set_title("Reference and Error Voltage Vectors")
axCW.set_title("Triangle Carrier & Vector Dwell Timings")
axCE.set_title("CESV trajectory")
# Set x and y axes limits
axSV.set_xlim(-0.1, 1.2)
axSV.set_ylim(-0.5, 0.5)
axCW.set_xlim(0, 2 * Ts)
axCW.set_ylim(0, 1 * Ts)
axCE.set_xlim(-0.15 * Ts, 0.15 * Ts)
axCE.set_ylim(-0.10 * Ts, 0.10 * Ts)
# Clear all axis ticks
axSV.set_xticks([])
axSV.set_yticks([])
axCW.set_xticks([])
axCW.set_yticks([])
axCE.set_xticks([])
axCE.set_yticks([])

# Plot initial waveforms
axSV.arrow(0, 0, V[1].real, V[1].imag, **V1a_params)
axSV.arrow(0, 0, V[2].real, V[2].imag, **V1a_params)
Vs_arw = axSV.arrow(0, 0, 0.50*V[1].real, 0.10*V[1].imag, **Vsa_params)
eV_arw = axSV.arrow(0, 0, 0.45*V[1].real, 0.05*V[1].imag, **eVa_params[0])
CESVline, = axCE.plot(0, 0, 'b')
CESVdot,  = axCE.plot(0, 0, 'ro')

# Define function for updating trace
def trace(num):
    # Global variables
    global CESVline, CESVdot, cesv, Vs_arw, eV_arw

    # Update angle of reference vector
    theta = int(num / (2 * Ts)) * thetaStep * (np.pi / 180)

    # Calculate reference vector
    Vs = Vm * np.exp((alpha + theta) * 1j)

    # Calculate dwell times of the three vectors
    tV = [0, 0, 0, 0]
    tV[1] = Ts * m * np.sin(delta - theta) / np.sin(delta)
    tV[2] = Ts * m * np.sin(theta) / np.sin(delta)
    tz = Ts - (tV[1] + tV[2])
    tV[0] = tz / 2
    tV[3] = tz / 2

    # Calculate error voltage vectors
    Ve = V - Vs

    # Calculate current error vectors for one half of carrier wave
    dI = Ve * tV

    # Calculate current time w.r.t start of carrier wave
    t = num % (2 * Ts)

    # Clear carrier subplot, at the start of each carrier wave
    if t == 0:
        axCW.lines = []
    if ((num % Ts) == 0):
        cesv = np.append(cesv, V[0])

    # Clear eV and replot Vs
    eV_arw.remove()
    Vs_arw.remove()
    Vs_arw = axSV.arrow(0, 0, Vs.real, Vs.imag, **Vsa_params)

    # Check direction of carrier - up or down
    if(t <= Ts):
        cw = t
        stV = list(accumulate(tV))
        sdI = list(accumulate(dI))
    else:
        cw = 2 * Ts - t
        stV = list(accumulate(tV[::-1]))[::-1]
        sdI = list(accumulate(dI[::-1]))[::-1]

    # Update CESV plot array, based on vector being applied
    if(t <= stV[0]):
        vctNo = 0
    elif (t <= stV[1]):
        vctNo = 1
    elif (t <= stV[2]):
        vctNo = 2
    elif (t <= Ts):
        vctNo = 3
    elif (t <= (stV[3] + Ts)):
        vctNo = 3
        stV[3] = stV[3] + Ts
    elif (t <= (stV[2] + Ts)):
        vctNo = 2
        stV[2] = stV[2] + Ts
    elif (t <= (stV[1] + Ts)):
        vctNo = 1
        stV[1] = stV[1] + Ts
    else:
        vctNo = 0
        stV[0] = stV[0] + Ts

    if((t + 1) > (stV[vctNo])):
        errI = sdI[vctNo]
    else:
        errI = cesv[-1] + Ve[vctNo]
    cesv = np.append(cesv, errI)

    # Update the plots
    axCW.plot(t, cw, **tm_params[vctNo])
    eV_arw = axSV.arrow(Vs.real, Vs.imag, Ve[vctNo].real, Ve[vctNo].imag, **eVa_params[vctNo])
    CESVline.set_data(cesv.real, cesv.imag)
    CESVdot.set_data(cesv[-1].real, cesv[-1].imag)

# Run the animation
ani = animation.FuncAnimation(fig, trace, int((360 / (N * thetaStep) + 1) * 2 * Ts), interval=1, fargs=(), repeat=False)

# uncomment next line for recording a video
# ani.save('cesv.mp4',fps=10,bitrate=1500)

plt.show()
